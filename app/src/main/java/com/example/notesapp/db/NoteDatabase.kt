package com.example.notesapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.notesapp.models.Note


@Database(
    entities = [Note::class],
    version = 3
)

@TypeConverters(Converters::class)
abstract class NoteDatabase: RoomDatabase() {

    abstract fun getNoteDao(): NoteDAO

    companion object{
        @Volatile
        private var instance: NoteDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK){
            instance ?: createDatabase(context).also { instance = it}
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                NoteDatabase::class.java,
                "note_db.db"
            )
                .fallbackToDestructiveMigration()
                .build()

    }

}