package com.example.notesapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapp.databinding.NoteItemBinding
import com.example.notesapp.models.Note

class NoteListAdapter(
    private val context: Context,
    private val clickListener: (Note) -> Unit
) : RecyclerView.Adapter<NoteListAdapter.ViewHolder>() {

    private var noteList: List<Note> = mutableListOf()

    private val differCallback = object : DiffUtil.ItemCallback<Note>() {
        override fun areItemsTheSame(
            oldItem: Note,
            newItem: Note
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: Note,
            newItem: Note
        ): Boolean {
            return oldItem == newItem
        }
    }
    private val differ = AsyncListDiffer(this, differCallback)

    fun setNewContent(listOfNotes: List<Note>) {
        noteList = listOfNotes
        differ.submitList(listOfNotes)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = NoteItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val noteItem = noteList[position]
        holder.bind(noteItem, clickListener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(private val binding: NoteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            noteItem: Note,
            clickListener: (Note) -> Unit
        ) {
            binding.apply {
                tvName.text = noteItem.name
                tvDate.text = noteItem.date.toString()
                root.setOnClickListener { clickListener(noteItem) }
            }
        }
    }


// это я не забыл, для себя оставил
//    fun clearList() {
//        val size = repoList.size
//        repoList.clear()
//        notifyItemRangeRemoved(0, size)
//    }

}