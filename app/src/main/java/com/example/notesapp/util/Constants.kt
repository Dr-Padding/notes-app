package com.example.notesapp.util

object Constants {
    const val NOTE_ID = "note_id"
    const val NOTE_NAME = "note_name"
    const val NOTE_TEXT = "note_text"
    const val EDIT_NOTE = "edit_note"
}