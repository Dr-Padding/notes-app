package com.example.notesapp.repository

import com.example.notesapp.db.NoteDatabase
import com.example.notesapp.models.Note

class NoteRepository(
    private val db: NoteDatabase
) {

    suspend fun insert(note: Note) = db.getNoteDao().insert(note)

    suspend fun updateNoteText(note: Note) = db.getNoteDao().updateNoteText(note)

    fun getSavedNotes() = db.getNoteDao().getAllNotes()

    suspend fun deleteNote(note: Note) = db.getNoteDao().deleteNote(note)

}