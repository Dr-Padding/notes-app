package com.example.notesapp.ui.fragments

import android.os.Bundle
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.notesapp.R
import com.example.notesapp.databinding.FragmentAddEditNoteBinding
import com.example.notesapp.db.NoteDatabase
import com.example.notesapp.repository.NoteRepository
import com.example.notesapp.ui.NoteListViewModel
import com.example.notesapp.ui.ViewModelProviderFactory
import com.example.notesapp.util.Constants.EDIT_NOTE
import com.example.notesapp.util.Constants.NOTE_ID
import com.example.notesapp.util.Constants.NOTE_NAME
import com.example.notesapp.util.Constants.NOTE_TEXT


class AddEditNoteFragment : Fragment(R.layout.fragment_add_edit_note) {

    private val binding by viewBinding(FragmentAddEditNoteBinding::bind)
    private lateinit var viewModel: NoteListViewModel
    var isEditMode = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBar()
        setToolbarTitle()
        setUpViewModel()
        initViews()
    }

    private fun setActionBar() {
        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbarAddEditNote)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        (requireActivity() as AppCompatActivity).supportActionBar
    }

    private fun setToolbarTitle() {
        val editNoteTitle = arguments?.getString(EDIT_NOTE)
        val addNoteTitle = getString(R.string.add_note)
        val finalTitle = editNoteTitle ?: addNoteTitle

        if (editNoteTitle != null) {
            isEditMode = true
        }

        val spannableTitle = SpannableString(finalTitle)
        spannableTitle.setSpan(AbsoluteSizeSpan(20, true), 0, finalTitle.length, 0)

        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            this.title = spannableTitle
        }
    }

    private fun setUpViewModel() {
        val noteDatabase = NoteDatabase.invoke(requireContext().applicationContext)
        val repository = NoteRepository(noteDatabase)
        val viewModelProviderFactory = ViewModelProviderFactory(repository)
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory)[NoteListViewModel::class.java]
    }

    private fun initViews() {
        binding.apply {

            if (isEditMode) {
                val editNoteId = arguments?.getLong(NOTE_ID)
                val editName = arguments?.getString(NOTE_NAME)
                nameEditText.setText(editName)
                nameEditText.isEnabled = false

                val editNoteText = arguments?.getString(NOTE_TEXT)
                noteEditText.setText(editNoteText)

                btnSave.setOnClickListener {
                    val updatedNoteText = noteEditText.text.toString()

                    if (updatedNoteText.isNotEmpty()) {
                        if (editNoteId != null && editName != null) {
                            viewModel.updateNoteText(editNoteId, editName, updatedNoteText)

                            findNavController().navigate(
                                R.id.action_addEditNoteFragment_to_noteListFragment
                            )
                        }
                    } else {
                        Toast.makeText(
                            requireContext(),
                            R.string.please_type_note_text,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else {

                btnSave.setOnClickListener {
                    val name = nameEditText.text.toString()
                    val noteContent = noteEditText.text.toString()

                    if (name.isNotEmpty() && noteContent.isNotEmpty()) {
                        viewModel.saveNote(name, noteContent)

                        findNavController().navigate(
                            R.id.action_addEditNoteFragment_to_noteListFragment
                        )

                    } else {
                        Toast.makeText(
                            requireContext(),
                            R.string.please_type_note,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
    }

}