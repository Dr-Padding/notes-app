package com.example.notesapp.ui.fragments

import android.os.Bundle
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.notesapp.R
import com.example.notesapp.databinding.FragmentViewNoteBinding
import com.example.notesapp.db.NoteDatabase
import com.example.notesapp.repository.NoteRepository
import com.example.notesapp.ui.NoteListViewModel
import com.example.notesapp.ui.ViewModelProviderFactory
import com.example.notesapp.util.Constants.EDIT_NOTE
import com.example.notesapp.util.Constants.NOTE_ID
import com.example.notesapp.util.Constants.NOTE_NAME
import com.example.notesapp.util.Constants.NOTE_TEXT


class ViewNoteFragment : Fragment(R.layout.fragment_view_note) {

    private val binding by viewBinding(FragmentViewNoteBinding::bind)
    lateinit var viewModel: NoteListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBar()
        setToolbarTitle()
        setUpViewModel()
        initViews()
    }

    private fun setActionBar() {
        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbarViewNote)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        (requireActivity() as AppCompatActivity).supportActionBar
    }

    private fun setToolbarTitle() {
        val title = getString(R.string.view_note)
        val spannableTitle = SpannableString(title)
        spannableTitle.setSpan(AbsoluteSizeSpan(20, true), 0, title.length, 0)

        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            this.title = spannableTitle
        }
    }

    private fun setUpViewModel() {
        val noteDatabase = NoteDatabase.invoke(requireContext().applicationContext)
        val repository = NoteRepository(noteDatabase)
        val viewModelProviderFactory = ViewModelProviderFactory(repository)
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory)[NoteListViewModel::class.java]
    }

    private fun initViews() {
        binding.apply {
            val noteId = arguments?.getLong(NOTE_ID)
            val noteName = arguments?.getString(NOTE_NAME)
            val noteText = arguments?.getString(NOTE_TEXT)
            tvName.text = noteName
            tvNote.text = noteText

            btnEdit.setOnClickListener {
                val bundle = bundleOf(
                    EDIT_NOTE to context?.getString(R.string.edit_note),
                    NOTE_ID to noteId,
                    NOTE_NAME to noteName,
                    NOTE_TEXT to noteText
                )
                findNavController().navigate(
                    R.id.action_viewNoteFragment_to_addEditNoteFragment,
                    bundle
                )
            }

            btnDelete.setOnClickListener {
                if (noteId != null) {
                    if (noteName != null) {
                        if (noteText != null) {
                            viewModel.deleteArticle(noteId, noteName, noteText)
                            findNavController().navigate(
                                R.id.action_viewNoteFragment_to_noteListFragment
                            )
                        }
                    }
                }
            }
        }
    }

}