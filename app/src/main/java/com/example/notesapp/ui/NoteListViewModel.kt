package com.example.notesapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.notesapp.models.Note
import com.example.notesapp.repository.NoteRepository
import kotlinx.coroutines.launch
import java.util.Calendar
import java.util.Date

class NoteListViewModel(
    private val noteRepository: NoteRepository
) : ViewModel() {

    fun saveNote(name: String, noteContent: String) = viewModelScope.launch {

        val currentDate = getCurrentDate()

        val note = Note(
            id = 0,
            name = name,
            content = noteContent,
            date = currentDate
        )
        noteRepository.insert(note)
    }

    fun getSavedNotes() = noteRepository.getSavedNotes()

    fun updateNoteText(id: Long, name: String, content: String) = viewModelScope.launch {

        val currentDate = getCurrentDate()

        val note = Note(
            id = id,
            name = name,
            content = content,
            date = currentDate
        )

        noteRepository.updateNoteText(note)
    }

    fun deleteArticle(id: Long, name: String, content: String) = viewModelScope.launch {

        val note = Note(
            id = id,
            name = name,
            content = content,
            date = null
        )

        noteRepository.deleteNote(note)
    }

    private fun getCurrentDate(): Date {
        return Calendar.getInstance().time
    }

}