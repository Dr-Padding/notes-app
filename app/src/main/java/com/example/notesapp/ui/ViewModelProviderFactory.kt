package com.example.notesapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.notesapp.repository.NoteRepository
import java.lang.RuntimeException

class ViewModelProviderFactory(
    private val noteRepository: NoteRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass) {
            NoteListViewModel::class.java -> NoteListViewModel(noteRepository) as T
            else -> throw RuntimeException()
        }
    }

}