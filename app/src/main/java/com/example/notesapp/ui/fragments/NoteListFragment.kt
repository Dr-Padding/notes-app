package com.example.notesapp.ui.fragments

import android.os.Bundle
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.notesapp.R
import com.example.notesapp.adapters.NoteListAdapter
import com.example.notesapp.databinding.FragmentNoteListBinding
import com.example.notesapp.db.NoteDatabase
import com.example.notesapp.repository.NoteRepository
import com.example.notesapp.ui.NoteListViewModel
import com.example.notesapp.ui.ViewModelProviderFactory
import com.example.notesapp.util.Constants.NOTE_ID
import com.example.notesapp.util.Constants.NOTE_NAME
import com.example.notesapp.util.Constants.NOTE_TEXT


class NoteListFragment : Fragment(R.layout.fragment_note_list) {

    private val binding by viewBinding(FragmentNoteListBinding::bind)
    lateinit var viewModel: NoteListViewModel
    private lateinit var adapter: NoteListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewModel()
        setActionBar()
        setToolbarTitle()
        initViews()
        setUpAdapter()
        setObservers()
    }

    private fun setUpViewModel() {
        val noteDatabase = NoteDatabase.invoke(requireContext().applicationContext)
        val repository = NoteRepository(noteDatabase)
        val viewModelProviderFactory = ViewModelProviderFactory(repository)
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory)[NoteListViewModel::class.java]
    }

    private fun setActionBar() {
        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbarNoteList)
        (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)
        (requireActivity() as AppCompatActivity).supportActionBar
    }

    private fun setToolbarTitle() {
        val title = getString(R.string.note_list)
        val spannableTitle = SpannableString(title)
        spannableTitle.setSpan(AbsoluteSizeSpan(20, true), 0, title.length, 0)

        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            this.title = spannableTitle
        }
    }


    private fun initViews() {
        binding.apply {
            btnCreateNote.setOnClickListener {
                findNavController().navigate(
                    R.id.action_noteListFragment_to_addEditNoteFragment
                )
            }
        }
    }

    private fun setUpAdapter() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter =
            NoteListAdapter(requireContext(), clickListener = { noteItem ->
                val bundle = bundleOf(
                    NOTE_ID to noteItem.id,
                    NOTE_NAME to noteItem.name,
                    NOTE_TEXT to noteItem.content
                )
                findNavController().navigate(
                    R.id.action_noteListFragment_to_viewNoteFragment,
                    bundle
                )
            })
        binding.recyclerView.adapter = adapter
    }

    private fun setObservers() {
        viewModel.getSavedNotes().observe(viewLifecycleOwner) { notes ->
            adapter.setNewContent(notes)

        }
    }

}