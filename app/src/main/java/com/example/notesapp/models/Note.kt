package com.example.notesapp.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.Date


@Entity(
    tableName = "notes"
)

data class Note(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String?,
    val content: String,
    val date: Date?,
) : Serializable
